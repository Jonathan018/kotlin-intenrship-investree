fun main() {
    print("Masukkan angka yang anda inginkan : ")
    val masukan_user = readLine();



    if (masukan_user != null) {
        for (i in 1..masukan_user.toInt()){
            if (i % 3 === 0 && i % 5 === 0){
                println("FizzBuzz")
                continue
            }else if (i % 3 === 0){
                println("Fizz")
                continue
            }else if (i % 5 === 0){
                println("Buzz")
                continue
            }
            println(i)
        }
    }
}