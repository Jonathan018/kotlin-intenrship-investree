package service

import model.MusicData

interface MusicService {

//    fun doLogin(username: String, password: String) : UserData?

    fun getAllMusic() : List<MusicData>

    fun getMusic(title: String) : MusicData?

    fun doPlayMusic(id: Int) : MusicData?

    fun getPlayedMusic() : MusicData?

    fun doAddMusic()
}