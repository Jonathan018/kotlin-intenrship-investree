import enums.PlayMusicMenu
import enums.SpotifyMenu
import feature.CredentialFeature
import feature.MusicFeature
import java.util.Scanner

// initialization
// lazy initialization

private val scanner = Scanner(System.`in`)
private lateinit var credentialFeature: CredentialFeature
private lateinit var musicFeature: MusicFeature


fun main() {
    setup()
    welcome()
}

fun welcome() {
    println()
    println()
    println("SELAMAT DATANG DI SPOTIFY")
    println("1. Login")
    println("2. Profile")
    println("3. Play Music")
    print("Pilih menu : ")

    when (scanner.nextLine()) {
        SpotifyMenu.LOGIN.id -> {
            credentialFeature.login()
        }
        SpotifyMenu.PROFILE.id -> {
            credentialFeature.getProfile()
        }
        SpotifyMenu.PLAYMUSIC.id -> {
            playMusic()
        }
        else -> println("Kode input tidak dapat di proses")

    }
    welcome()
}

fun playMusic() {
    println()
    println()
    println()
    println("Menu Play Music")
    println("1. Daftar Lagu")
    println("2. Cari Lagu")
    println("0. Kembali ke menu sebelumnya")
    print("Pilih menu : ")

    when (scanner.nextLine()) {
        PlayMusicMenu.FINDALLMUSIC.id -> {
            musicFeature.allMusic()
        }
        PlayMusicMenu.SEARCHMUSIC.id -> {
            musicFeature.searchMusic()
        }
        PlayMusicMenu.BACK.id -> {
            welcome()
        }
        else -> println("Kode input tidak dapat di proses")
    }
    playMusic()
}


fun setup() {
    credentialFeature = CredentialFeature(scanner)
    musicFeature = MusicFeature(scanner)
}