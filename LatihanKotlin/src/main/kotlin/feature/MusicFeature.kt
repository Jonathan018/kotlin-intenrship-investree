package feature

import model.MusicData
import service.CredentialService
import service.CredentialServiceImpl
import service.MusicService
import service.MusicServiceImpl
import java.util.Scanner

class MusicFeature(private val scanner: Scanner) {
    private val credentialService: CredentialService = CredentialServiceImpl()
    private val musicService: MusicService = MusicServiceImpl()

    fun searchMusic() {

        print("Song Title : ")
        val inputSongName = scanner.nextLine()

        musicService.getMusic(inputSongName)?.let {
            printMusicData(it)
        }

    }

    fun allMusic() {
        musicService.getAllMusic()?.let {
            printAllMusic(it)
        }
    }

    private fun printAllMusic(musicData: List<MusicData>) {
        for (i in musicData) {
            println("${i.id}. ${i.title}")
        }
    }


    private fun printMusicData(musicData: MusicData) {
        println("================================")
        println("Title : ${musicData.title}")
        println("Artist : ${musicData.artist}")
        println("Release : ${musicData.release}")
        println("Genre : ${musicData.genre}")
        println("================================")
    }

}