package feature

import database.UserDatabase
import database.UserDatabaseImpl
import model.UserData
import service.CredentialService
import service.CredentialServiceImpl
import java.util.Scanner

class CredentialFeature(private val scanner : Scanner) {
    private val credentialService: CredentialService = CredentialServiceImpl()

    fun login() {
        print("Username : ")
        val inputUsername = scanner.nextLine()


        print("Password : ")
        val inputPassword = scanner.nextLine()

        credentialService.doLogin(inputUsername,inputPassword)?.let{
        printWelcomeUser(it)
        }
    }

    fun getProfile() {
        credentialService.getLoggedInUser()?.let {
            printUserData(it)
        }?: print("anda belum login")

    }

    private fun printUserData(userData: UserData) {
        println("================================")
        println("Username : ${userData.username}")
        println("Nama : ${userData.name}")
        println("Address : ${userData.address}")
        println("================================")
    }

    private fun printWelcomeUser(userData: UserData){
        println("================================")
        println("Kamu berhasil login")
        println("Hai ${userData.name}, bagai mana kabarmu hari ini?")
        println("================================")
    }
}