package model

data class MusicData(
    val id: Int,
    val title: String? = null,
    val artist: String? = null,
    val release: String? = null,
    val genre: String? = null
)