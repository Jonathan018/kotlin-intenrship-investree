package database

import model.MusicData

interface MusicDatabase {

    fun findAllMusic(): List<MusicData>

    fun findMusic(title: String): MusicData?

    fun findMusic(id: Int): MusicData?

    fun addMusic(data: MusicData)
}