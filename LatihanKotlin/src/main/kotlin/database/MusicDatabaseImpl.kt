package database

import model.MusicData

class MusicDatabaseImpl : MusicDatabase {

    private val musicList = mutableListOf(
        MusicData(1,"Rap God","Eminem","2013","hip hop"),
        MusicData(2,"Help","Pink Guy", "2017","hip hop"),
        MusicData(3, "Hit Em Up","Tupac Shakur","1996", "hip hop")

    )

    override fun findAllMusic(): List<MusicData> = musicList

    override fun findMusic(title: String): MusicData? = musicList.find {
        it.title.equals(title, true)
    }

    override fun findMusic(id: Int): MusicData? = musicList.find {
        it.id.equals(id)
    }

    override fun addMusic(data: MusicData) {
        musicList.add(data)
    }
}