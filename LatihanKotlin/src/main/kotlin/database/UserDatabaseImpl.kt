package database

import model.UserData

class UserDatabaseImpl : UserDatabase {

    private val userList = mutableListOf(
        UserData(1, "Jonathan", "jonathan", "jonathanganteng", "Suzuran"),
        UserData(2, "Daud", "daud", "daudganteng", "Rindow"),
        UserData(3, "Loise", "loise", "loiseganteng", "Housen")
    )

    override fun findAllUser(): List<UserData> = userList

    override fun findUser(username: String): UserData? = userList.find {
        it.username.equals(username, true)
    }
//        for (position in 0..userList.size){
//            val selectedUserToCheck = userList[position]
//            if (selectedUserToCheck.username.equals(username,true)){
//                return selectedUserToCheck
//            }
//        }
//        return null

//        userList.forEach { selectedUserToCheck ->
//            if (selectedUserToCheck.username.equals(username, true)) {
//                return selectedUserToCheck
//            }
//        }
//
//        return null

/*        return userList.find(username: String?) : UserData? = userList.find{
            it.username.equals(username, true)
        }*/



    override fun addUser(data: UserData) {
       userList.add(data)
    }

}