package com.jonathan.demopejabat

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class DemoPejabatApplication

fun main(args: Array<String>) {
	runApplication<DemoPejabatApplication>(*args)
}
