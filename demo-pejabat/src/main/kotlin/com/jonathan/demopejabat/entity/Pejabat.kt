package com.jonathan.demopejabat.entity

import javax.persistence.*


@Entity
@Table(name = "pejabats")
data class Pejabat(

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Int? = null,

    @Column(name = "nama")
    var nama: String? = null,

    @Column(name = "jeniskelamin")
    var jeniskelamin: String? = null,

    @Column(name = "umur")
    var umur: Int? = null,

    @Column(name = "pendidikan")
    var pendidikan: String? = null


)