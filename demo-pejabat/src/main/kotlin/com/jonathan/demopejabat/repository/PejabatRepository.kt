package com.jonathan.demopejabat.repository

import com.jonathan.demopejabat.entity.Pejabat
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository


@Repository
interface PejabatRepository: JpaRepository<Pejabat, Int>