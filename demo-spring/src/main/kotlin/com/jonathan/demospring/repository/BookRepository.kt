package com.jonathan.demospring.repository

import com.jonathan.demospring.entity.Book
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository


@Repository
interface BookRepository: JpaRepository<Book, Int>