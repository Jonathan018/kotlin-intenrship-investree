package com.jonathan.demospring

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*


@RestController
@RequestMapping("/kalkulator")
class KalkulatorController(
    private val kalkulator: Kalkulator
) {


    /*
       URL : /kalkulator/tambah?angka1=10&angka2=5
    */
    @GetMapping("/tambah")
    fun actionTambah(
        @RequestParam("angka1") angka1: Int,
        @RequestParam("angka2") angka2: Int,
    ): ResponseEntity<String> {
        val result = kalkulator.tambah(angka1, angka2)
        val response = "$angka1 + $angka2 = $result"
        return ResponseEntity(response, HttpStatus.OK)

    }


//    Latihan path variable

    @GetMapping("/pangkat/{number}")
    fun dapatkanPangkat(
        @PathVariable number: Int
    ): ResponseEntity<Int> {
        return ResponseEntity(number * number, HttpStatus.OK)
    }

    // Latihan Header
    // /kalkilator/luas-segitiga

    @GetMapping("/luas-segitiga")
    fun dapatkanLuasSeigitiga(
        @RequestHeader("alas") alas: Int,
        @RequestHeader("tinggi") tinggi: Int
    ) : ResponseEntity<Int>{
        val luas = (alas * tinggi) / 2
        return ResponseEntity(luas,HttpStatus.OK)
    }
        /*
            Latihan body request
            localhost:8080/kalkulator/volume-tabung
        */
    @PostMapping("/volume-tabung")
        fun getVolumeTabung(
            @RequestBody request: VolumeTabungRequest
        ): ResponseEntity<Double>{
            val luasAlas = 3.14 * request.jariJariAlas * request.jariJariAlas
            val volume = luasAlas * request.tinggi
            return ResponseEntity(volume,HttpStatus.OK)
        }

    data class VolumeTabungRequest(
        val jariJariAlas:Int,
        val tinggi:Int
    )


}
